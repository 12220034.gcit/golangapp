package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	var stud model.Student

	// fmt.Println(stud)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)

	if err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "invalid json body")
	}

	saveErr := stud.Create()
	if saveErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, saveErr.Error())
	} else {
		httpResp.RespondwithJSON(w, http.StatusCreated, map[string]string{"message": "student data added"})
	}
}

// Helper function to convert sid string to int
func getUserId(sid string) (int64, error) {
	stdd, err := strconv.ParseInt(sid, 10, 64)
	return stdd, err
}

func GetStud(w http.ResponseWriter, r *http.Request) {
	// ssid of type string
	ssid := mux.Vars(r)["stdid"]

	// stdid is of type int
	stdid, _ := getUserId(ssid)
	// Declaring and assignings
	stud := model.Student{StdId: stdid}
	getErr := stud.Read()
	if getErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "student not found")
	} else {
		httpResp.RespondwithJSON(w, http.StatusOK, stud)
	}
}

func UpdateStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	fmt.Println("")
	old_sid := mux.Vars(r)["sid"]
	old_stdId, idErr := getUserId(old_sid)

	if idErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var stud model.Student

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "invalid jsonbody")
		return
	}
	defer r.Body.Close()

	updateErr := stud.Update(old_stdId)

	if updateErr != nil {
		switch updateErr {

		case sql.ErrNoRows:

			httpResp.RespondwithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondwithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.RespondwithJSON(w, http.StatusOK, stud)
	}
}

// Handle func -- gets executed first
func DeleteStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	sid := mux.Vars(r)["sid"]
	stdid, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdId: stdid}
	if err := s.Delete(); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	students, getErr := model.GetAllStuds()
	if getErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, students)
}
