package controller

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("hello world"))
	if err != nil {
		fmt.Println("error: ", err)
	}
}

func ParameterHandler(w http.ResponseWriter, r *http.Request) {
	p := mux.Vars(r)
	// fmt.Println(p)
	name := p["myname"]
	_, err := w.Write([]byte("my name is " + name))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
