package model

import "myapp/dataStore/postgres"

type Admin struct {
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

const (
	queryInsertAdmin = `INSERT INTO admin (firstname, lastname, email, password) VALUES ($1, $2, $3, $4);`
	queryGetAdmin    = `SELECT email, password FROM admin WHERE email=$1 and password=$2;`
	// queryUpdateAdmin     = `UPDATE admin SET firstname=$1, lastname=$2, email=$3, password=$4 WHERE stdid=$5 RETURNING email;`
	// queryDeleteUserAdmin = `DELETE FROM admin WHERE email=$1 RETURNING email;`
)

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.FirstName, adm.LastName, adm.Email, adm.Password)
	return err
}

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}
