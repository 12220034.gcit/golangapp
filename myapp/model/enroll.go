package model

import "myapp/dataStore/postgres"

type Enroll struct {
	Stdd          int    `json: "stdid"`
	CourseId      string `json:"cid"`
	Date_Enrolled string `json: "date"`
}

const queryEnrolled = "INSERT INTO enroll(std_id, course_id, date_enrolled) VALUES($1, $2, $3) RETURNING std_id"
const queryGetEnroll = "SELECT std_id, course_id, date_enrolled FROM enroll WHERE std_id=$1 and course_id=$2;"
const queryDeleteEnroll = "DELETE FROM enroll WHERE std_id=$1 and course_id=$2 RETURNING std_id;"

func (e *Enroll) EnrollStud() error {
	row := postgres.Db.QueryRow(queryEnrolled, e.Stdd, e.CourseId, e.Date_Enrolled)
	err := row.Scan(e.Stdd)
	return err
}

func (e *Enroll) Get() error {
	return postgres.Db.QueryRow(queryGetEnroll,
		e.Stdd, e.CourseId).Scan(&e.Stdd, &e.CourseId, &e.Date_Enrolled)
}

func GetAllEnrolls() ([]Enroll, error) {
	rows, getErr := postgres.Db.Query("SELECT std_id, course_id, date_enrolled from enroll;")
	if getErr != nil {
		return nil, getErr
	}
	//create a slice of type course
	enrolls := []Enroll{}

	for rows.Next() {
		var e Enroll
		dbErr := rows.Scan(&e.Stdd, &e.CourseId, &e.Date_Enrolled)
		if dbErr != nil {
			return nil, dbErr
		}
		enrolls = append(enrolls, e)
	}
	rows.Close()
	return enrolls, nil
}

func (e *Enroll) Delete() error {
	row := postgres.Db.QueryRow(queryDeleteEnroll, e.Stdd, e.CourseId)
	err := row.Scan(&e.Stdd)
	return err
}
