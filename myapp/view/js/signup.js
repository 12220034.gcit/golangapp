function signUp(){
    // data to be sent to the POST request
    var _data = {
        firstName : document.getElementById("fname").value,
        lastName : document.getElementById("lname").value,
        email : document.getElementById("email").value,
        password : document.getElementById("pw1").value,
        pw : document.getElementById("pw2").value    }
    
    if (_data.password !== _data.pw){
    alert("PASSWORD Doesn't match!")
    return
    }
    fetch('/signup', {
    method: "POST",
    body: JSON.stringify(_data),
    headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
    if (response.status == 201){
        window.open("index.html", "_self")
        // window.location.href(".html", "_self")
    }
    })
}